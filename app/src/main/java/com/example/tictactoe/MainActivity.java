package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    boolean gameActive = true;
    // -1 - null
    // 1 - X
    // 0 - O
    int activePlayer = 1;
    int[] gameState = {-1, -1, -1, -1, -1, -1, -1, -1, -1};

    int[][] winPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};
    public static int gameMoves = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView player = findViewById(R.id.mainActivity_winner_iv);
        player.setImageResource(R.drawable.xplay);
    }

    public void playerMove(View view) {
        ImageView img = (ImageView) view;
        int tappedImage = Integer.parseInt(img.getTag().toString());
        ImageView player = findViewById(R.id.mainActivity_winner_iv);

        // if the tapped image is empty
        if (gameState[tappedImage] == -1) {
            gameMoves++;

            if (gameMoves == 9) {
                gameActive = false;
            }

            gameState[tappedImage] = activePlayer;

            if (activePlayer == 0) {
                activePlayer = 1;
                img.setImageResource(R.drawable.o);
                player.setImageResource(R.drawable.xplay);

            } else {
                activePlayer = 0;
                img.setImageResource(R.drawable.x);
                player.setImageResource(R.drawable.oplay);
            }

        }
        int flag = 0;
        ImageView winnerLine = findViewById(R.id.mainActivity_result_iv);

        // Check if any player has won
        for (int i =0; i< winPositions.length && flag==0; i++) {
            if (gameState[winPositions[i][0]] == gameState[winPositions[i][1]] &&
                    gameState[winPositions[i][1]] == gameState[winPositions[i][2]] &&
                    gameState[winPositions[i][0]] != -1) {
                flag = 1;

                // mark the winner
                switch(i){
                    case 0:
                        winnerLine.setImageResource(R.drawable.mark0);
                        break;
                    case 1:
                        winnerLine.setImageResource(R.drawable.mark1);
                        break;
                    case 2:
                        winnerLine.setImageResource(R.drawable.mark2);
                        break;
                    case 3:
                        winnerLine.setImageResource(R.drawable.mark3);
                        break;
                    case 4:
                        winnerLine.setImageResource(R.drawable.mark4);
                        break;
                    case 5:
                        winnerLine.setImageResource(R.drawable.mark5);
                        break;
                    case 6:
                        winnerLine.setImageResource(R.drawable.mark6);
                        break;
                    case 7:
                        winnerLine.setImageResource(R.drawable.mark7);
                        break;
                    default:
                        break;
                }

                gameActive = false;

                Button playAgain = (Button)findViewById(R.id.mainActivity_playAgain_btn);
                playAgain.setVisibility(View.VISIBLE);

                if (gameState[winPositions[i][0]] == 0) {
                    player.setImageResource(R.drawable.owin);
                } else {
                    player.setImageResource(R.drawable.xwin);
                }

            }
        }

        if (gameMoves == 9 && flag == 0) {
            player.setImageResource(R.drawable.nowin);
            Button playAgain = (Button)findViewById(R.id.mainActivity_playAgain_btn);
            playAgain.setVisibility(View.VISIBLE);
        }
    }

    public void gameReset(View view) {
        gameActive = true;
        activePlayer = 1;
        gameMoves = 0;
        Button playAgain = (Button)findViewById(R.id.mainActivity_playAgain_btn);
        playAgain.setVisibility(View.INVISIBLE);
        for (int i = 0; i < gameState.length; i++) {
            gameState[i] = -1;
        }

        ((ImageView) findViewById(R.id.mainActivity_1_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_2_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_3_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_4_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_5_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_6_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_7_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_8_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_9_iv)).setImageResource(0);
        ((ImageView) findViewById(R.id.mainActivity_result_iv)).setImageResource(0);
        ImageView player = findViewById(R.id.mainActivity_winner_iv);
        player.setImageResource(R.drawable.xplay);
    }

    }

